# Sticks and stones game
Game invented by my friends, that I love, so decided to implement. The plan is to develop
the rules engine in rust, then develop a lovely chess.com style front end for people to 
play online with.

## Rules
I know them, will right them down later (dw a good tutorial is coming)

## Implementation

```rust
struct Game
    rocks: Vec<Node>,
    sticks: UndirectedGraph,
```

Player colours need to be changed to be visually distinct for small values of player_id

### Stick (Graph)
- Will be undirected
- Will need to detect when a loop is formed (an area is created)
- Will need to check if this area is larger than 2 units
    - Only have to check if the loops form a "unit" triangle
- Will need to check that the lines don't overlap
- Make sure each line is not too long
- Don't allow starting a stick from an opponents rock
- Make sure the stick starts from another stick

#### Check if a loop is formed
1. re-check the entire graph
2. check for each move, as the graph is constructed

Turns out these two strats are the same. There is no fancy algo that any can take advantage of 
that the other can't.

I have decided to use 1 as it is more generic, and the performance cost of parsing the whole graph will be small, as the nature of the game results in small games anyway.

I am going to use the depth first algo described here <https://www.geeksforgeeks.org/detect-cycle-undirected-graph/> (it may or may not have been the first duck search response).

### Rock
- Check that the rock does not replace another rock
- Check that the player has not played a rock in the previous 2 moves
