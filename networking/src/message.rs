pub struct Message {
    player: u32,
    message: Messages,
}

pub enum Messages {
    NewPlayer(u32),
    Move(),
}
