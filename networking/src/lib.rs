//! This crate is intended to wrap the Game object from Sticks and Stones to
//! handle the web socket communication.

mod message;

pub use message::Message;
pub use message::Messages;