use std::fmt::Display;

/// This struct mainly exists to encode move data for serialization across the
/// network
/// 
#[cfg(feature = "serde1")]
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Clone)]
#[cfg_attr(feature = "serde1", derive(Serialize, Deserialize))]
pub enum Move<Node, Player> {
    Stick(Stick<Node, Player>),
    Rock(Rock<Node, Player>),
}

impl<Node: Display, Player: Display> Display for Move<Node, Player> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let output = match self {
            Move::Stick(stick) => stick.to_string(),
            Move::Rock(rock) => rock.to_string(),
        };

        write!(f, "{}", output)
    }
}

impl<Node, Player> From<Stick<Node, Player>> for Move<Node, Player> {
    fn from(value: Stick<Node, Player>) -> Self {
        Move::Stick(value)
    }
}

impl<Node, Player> From<Rock<Node, Player>> for Move<Node, Player> {
    fn from(value: Rock<Node, Player>) -> Self {
        Move::Rock(value)
    }
}

#[derive(Debug, PartialEq, Clone)]
#[cfg_attr(feature = "serde1", derive(Serialize, Deserialize))]
pub struct Stick<Node, Player> {
    pub player: Player,
    pub start: Node,
    pub end: Node,
}

impl<Node, Player> Stick<Node, Player> {
    pub fn new(player: Player, start: Node, end: Node) -> Self {
        Self { player, start, end }
    }
}

impl<Node: Display, Player: Display> Display for Stick<Node, Player> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}: {} -> {}", self.player, self.start, self.end)
    }
}

#[derive(Debug, PartialEq, Clone)]
#[cfg_attr(feature = "serde1", derive(Serialize, Deserialize))]
pub struct Rock<Node, Player> {
    pub player: Player,
    pub location: Node,
}

impl<Node, Player> Rock<Node, Player> {
    pub fn new(player: Player, location: Node) -> Self {
        Self { player, location }
    }
}

impl<Node: Display, Player: Display> Display for Rock<Node, Player> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}: {}", self.player, self.location)
    }
}
