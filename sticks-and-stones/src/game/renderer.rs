use std::path::Path;

use crate::{
    game::{Game, Vec2},
    graph::TableGraph,
};

use plotters::prelude::*;
use random_color::RandomColor;

use super::MoveInvalid;

impl Game<TableGraph<Vec2>, Vec2> {
    pub fn render(&mut self, file_name: &Path) -> Result<&mut Self, Box<dyn std::error::Error>> {
        let root_area = BitMapBackend::new(file_name, (1080, 1080)).into_drawing_area();

        root_area.fill(&WHITE)?;

        let root_area = root_area.titled("RuboGubo vs. Mr. Nobody", ("sans-serif", 60))?;

        let mut cc = ChartBuilder::on(&root_area)
            .margin(5)
            .set_all_label_area_size(50)
            .caption("Sticks&Stones", ("sans-serif", 40))
            .build_cartesian_2d(-1.0..2.0_f64, -1.0..2.0_f64)?;

        cc.configure_mesh()
            .max_light_lines(0)
            .x_label_formatter(&|v| format!("{:.1}", v))
            .y_label_formatter(&|v| format!("{:.1}", v))
            .draw()?;

        cc.configure_series_labels().border_style(BLACK).draw()?;

        /*
        // It's possible to use a existing pointing element
         cc.draw_series(PointSeries::<_, _, Circle<_>>::new(
            (-3.0f32..2.1f32).step(1.0).values().map(|x| (x, x.sin())),
            5,
            Into::<ShapeStyle>::into(&RGBColor(255,0,0)).filled(),
        ))?;*/

        // Otherwise you can use a function to construct your pointing element yourself
        // cc.draw_series(PointSeries::of_element(
        //     (-3.0f32..2.1f32).step(1.0).values().map(|x| (x, x)),
        //     5,
        //     ShapeStyle::from(&RED).filled(),
        //     &|coord, size, style| {
        //         EmptyElement::at(coord)
        //             + Circle::new((0, 0), size, style)
        //             + Text::new(format!("{:?}", coord), (0, 15), ("sans-serif", 15))
        //     },
        // ))?;

        // Sticks
        for (start, ends) in self.get_sticks().clone().into_iter() {
            for (end, player) in ends {
                let coords: Vec<(f64, f64)> = vec![start.clone().into(), end.into()];
                cc.draw_series(LineSeries::new(
                    coords.into_iter(),
                    get_player_style(player),
                ))?;
            }
        }

        // Rocks
        cc.draw_series(PointSeries::of_element(
            self.get_rocks().into_iter(),
            5,
            ShapeStyle::from(&RED).filled(),
            &|point_data, size, _| {
                let coord = point_data.0.clone().into();
                let colour = get_player_style(point_data.1);
                EmptyElement::at(coord)
                    + Circle::new((0, 0), size, ShapeStyle::from(colour).filled())
                // + Text::new(format!("{:?}", coord), (0, 15), ("sans-serif", 15))
            },
        ))?;

        // To avoid the IO failure being ignored silently, we manually call the present function
        root_area.present().expect("Unable to write result to file, please make sure the output dir exists under current dir");
        println!("Result has been saved to {:?}", file_name);
        Ok(self)
    }

    #[allow(unused)]
    fn test_data() -> Result<Self, MoveInvalid> {
        let mut game = Self::default();
        game.play_rock(Vec2(0, 0), 0)?
            .play_rock(Vec2(0, 5), 1)?
            .play_rock(Vec2(3, 2), 0)?
            .play_rock(Vec2(1, 2), 1)?
            .play_stick(Vec2(0, 0), Vec2(1, 0), 0)?
            .play_stick(Vec2(1, 0), Vec2(1, 1), 1)?
            .play_stick(Vec2(1, 1), Vec2(0, 0), 0)?;

        Ok(game)
    }
}

fn get_player_style(player: u32) -> ShapeStyle {
    let colour = RandomColor::default().seed(player).to_rgb_array();
    ShapeStyle {
        color: RGBColor(colour[0], colour[1], colour[2]).into(),
        filled: true,
        stroke_width: 3,
    }
}

#[test]
fn test_renderer() {
    let mut game: Game<TableGraph<Vec2>, Vec2> = Game::test_data().unwrap();

    game.render(Path::new("output/test/t.png")).unwrap();
}
