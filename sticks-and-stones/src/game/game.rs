use err_into::ErrorInto;
use std::{
    collections::HashMap,
    fmt::{Debug, Display},
    hash::Hash,
    ops::Sub,
};

#[cfg(feature = "serde1")]
use serde::{Deserialize, Serialize};

use crate::graph::UndirectedWeightedGraphTrait;

use super::{
    game_moves::{Move, Rock, Stick}, vec2::VectorMethods, MoveInvalid, RockInvalid, StickInvalid
};

#[derive(Debug, PartialEq, Clone, Default)]
#[cfg_attr(feature = "serde1", derive(Serialize, Deserialize))]
pub struct Game<Sticks, Node, Player>
where
    Player: Eq + Hash,
    Sticks: UndirectedWeightedGraphTrait<Node, Player>,
{
    rocks: Vec<(Node, Player)>,
    sticks: Sticks,

    rock_placed_in_row_table: HashMap<Player, u32>,
    player_order: Vec<Player>,
    // How far through the list above we are.
    turn_progress: usize,

    move_history: Vec<Move<Node, Player>>,
}

impl<
    Stick: UndirectedWeightedGraphTrait<Node, u32> + Default, 
    Node,
> Game<Stick, Node, u32> {
    fn testing() -> Self {
        Self {
            rocks: Default::default(),
            sticks: Default::default(),
            rock_placed_in_row_table: Default::default(),
            player_order: vec![0, 1],
            turn_progress: Default::default(),
            move_history: Default::default(),
        }
    }
}

impl<Sticks, Node, Player> Game<Sticks, Node, Player>
where
    Node: PartialEq + VectorMethods + Sub + Clone + Hash + Eq + Default + Debug,
    Sticks: UndirectedWeightedGraphTrait<Node, Player> + Default,
    Player: Eq + Hash + Default + Clone + Debug,
{
    pub fn new(player_order: Vec<Player>) -> Self {
        Self {
            player_order,
            ..Default::default()
        }
    }

    pub fn from_moves(player_order: Vec<Player>, moves: Vec<Move<Node, Player>>) -> Result<Self, MoveInvalid<Player>> {
        let mut game = Self::new(player_order);

        for game_move in moves {
            game.play_move(game_move)?;
        }

        Ok(game)
    }

    pub fn play_move(&mut self, game_move: Move<Node, Player>) -> Result<&mut Self, MoveInvalid<Player>> {
        Ok(match game_move {
            Move::Stick(stick) => self.play_move_stick(stick)?,
            Move::Rock(rock) => self.play_move_rock(rock)?,
        })
    }

    pub fn play_move_stick(&mut self, stick_move: Stick<Node, Player>) -> Result<&mut Self, MoveInvalid<Player>> {
        self.play_stick(stick_move.start, stick_move.end, &stick_move.player)
    }

    pub fn play_move_rock(&mut self, rock_move: Rock<Node, Player>) -> Result<&mut Self, MoveInvalid<Player>> {
        self.play_rock(rock_move.location, rock_move.player)
    }

    fn next_turn(&mut self, attempted_player: &Player) -> Result<usize, MoveInvalid<Player>> {
        let player_turn = &self.player_order[self.turn_progress];
        if player_turn != attempted_player {
            return Err(MoveInvalid::NotPlayerTurn {
                attempted_player: attempted_player.clone(),
                player_turn: player_turn.clone(),
            });
        }

        self.turn_progress = (self.turn_progress + 1) % self.player_order.len();
        Ok(self.turn_progress)
    }

    /// See this link <https://www.desmos.com/calculator/9qhfqzbiji> to see how the overlap is
    /// detected
    pub fn play_stick(
        &mut self,
        start: Node,
        end: Node,
        player: &Player,
    ) -> Result<&mut Self, MoveInvalid<Player>> {
        self.next_turn(&player)?;

        if !start.is_adjacent(&end) {
            return Err(StickInvalid::TooLong).err_into();
        }

        // rock check - there is a rock there, but it's not yours
        if self
            .rocks
            .iter()
            .find(|(node, other_player)| player != other_player && node == &start)
            .is_some()
        {
            return Err(StickInvalid::OpponentRockOnStart).err_into();
        }

        // line check - does it start on a line
        if !self.sticks.get_all_parents().is_empty() {
            // exception for the start of the game
            if let Some(children) = self.sticks.get_children(&start) {
                if children.is_empty() {
                    return Err(StickInvalid::StartNotOnLine).err_into();
                }
            } else {
                return Err(StickInvalid::StartNotOnLine).err_into();
            }
        }

        // overlap check
        if let Some([targets_1, targets_2]) = start.get_overlap_points(&end) {
            self.check_overlap(&targets_1, &targets_2)?;
            self.check_overlap(&targets_2, &targets_1)?; // Not sure if this needs to be run twice.
        }

        self.sticks.add_connection(&start, &end, player);

        // loop detection
        // -> area check
        // This ofc has to come after the connection is made (spend 3h debuting this one :( )
        let mut visited = Vec::default();
        let mut loop_contenders = Vec::default();
        self.recursive_loop_detection(
            &Node::default(),
            &mut visited,
            &mut loop_contenders,
            &Node::default(),
        )?;

        // return &mut Self so that it can be placed in the middle of a construction sequence.
        self.move_history
            .push(Move::Stick(Stick { player: player.clone(), start, end }));
        Ok(self)
    }

    /// Does not cover the case where two loops are formed, and the largest needs to be found
    /// this could be fixed by parsing the loop_contenders list after it has been generated
    /// just make sure to mark where loops happen, by adding a duplicate of the joint at
    /// the appropriate location in the stack.
    fn recursive_loop_detection<'a>(
        &'a self,
        current: &'a Node,
        visited: &mut Vec<&'a Node>,
        loop_contenders: &mut Vec<&'a Node>,
        parent: &'a Node,
    ) -> Result<(), StickInvalid<Player>> {
        visited.push(current);
        loop_contenders.push(current);

        println!("New recursion");
        println!("  {:?}", current);
        println!("  lc: {:?}", loop_contenders);
        println!("  v:  {:?}", visited);

        if let Some(children) = self.sticks.get_children(current) {
            println!("  c:  {:?}", children);
            for (child, player) in children {
                if visited.iter().find(|x| x == &&child).is_none() {
                    self.recursive_loop_detection(child, visited, loop_contenders, current)?;
                    // None of your children where part of a loop, so you cant be either, as
                    // it is a stack, you can pop, as all the children will have removed themselves
                    // already.
                    loop_contenders.pop();
                } else if child != parent {
                    println!("  New Loop");
                    println!("    {:?}", current);
                    println!("    c: {:?}", child);
                    println!("    lc: {:?}", loop_contenders);
                    println!("    v:  {:?}", visited);
                    println!("    ev: {:?}", visited.iter().find(|x| x == &&child));
                    // Loop found!
                    // we now know that the child node is the point at which
                    // the loop joins itself i.e. the "joint"
                    let joint = child;

                    // dbg!(joint, &loop_contenders, &visited);

                    let join_index = match loop_contenders.iter().position(|x| x == &joint) {
                        Some(x) => x,
                        None => continue, // this is the relic of a disallowed loop
                    };

                    // if the loop is a unit triangle
                    // unit triangles don't count as an end game.
                    if loop_contenders[join_index..].len() == 3 {
                        continue;
                    }

                    // dbg!(joint, &loop_contenders[join_index..]);

                    return Err(StickInvalid::AreaCreated(player.clone()));
                }
            }
        } else {
            // If you have no children, then you have no chance of forming a loop
            loop_contenders.pop();
        }

        println!("Exiting Recursion");

        return Ok(()); // No loops
    }

    fn check_overlap(&self, target_1: &Node, target_2: &Node) -> Result<(), StickInvalid<Player>> {
        let empty_vec = vec![];
        if self
            .sticks
            .get_children(target_1)
            .unwrap_or_else(|| &empty_vec)
            .iter()
            .find(|(point, _player)| point == target_2)
            .is_some()
        {
            return Err(StickInvalid::Overlap);
        }

        Ok(())
    }

    pub fn play_rock(&mut self, start: Node, player: Player) -> Result<&mut Self, MoveInvalid<Player>> {
        self.next_turn(&player)?;

        if self
            .rocks
            .iter()
            .map(|(node, _player_id)| node)
            .find(|x| x == &&start)
            .is_some()
        {
            return Err(RockInvalid::PositionOccupied).err_into();
        }

        let rocks_placed_in_row = self
            .rock_placed_in_row_table
            .get(&player)
            .cloned()
            .unwrap_or_default();
        if rocks_placed_in_row >= 2 {
            return Err(RockInvalid::PlacedTooManyRocks(rocks_placed_in_row)).err_into();
        }

        self.rock_placed_in_row_table
            .entry(player.clone())
            .and_modify(|x| *x += 1)
            .or_default();

        self.move_history.push(Move::Rock(Rock {
            player: player.clone(),
            location: start.clone(),
        }));
        self.rocks.push((start, player));

        Ok(self)
    }

    pub fn get_rocks(&self) -> &Vec<(Node, Player)> {
        &self.rocks
    }

    pub fn get_sticks(&self) -> &Sticks {
        &self.sticks
    }

    pub fn get_move_history(&self) -> &Vec<Move<Node, Player>> {
        &self.move_history
    }
}

impl<Sticks, Node, Player> Display for Game<Sticks, Node, Player>
where
    Player: Eq + Hash + Display,
    Node: Display,
    Sticks: UndirectedWeightedGraphTrait<Node, Player> + Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut output = "".to_string();
        output += "Game";
        for game_move in self.move_history.iter() {
            output += &game_move.to_string();
            output += "\n";
        }
        write!(f, "{}", output)
    }
}
