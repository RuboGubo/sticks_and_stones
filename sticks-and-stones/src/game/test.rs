use std::path::Path;

use crate::{game::MoveInvalid, graph::TableGraph};
use pretty_assertions::assert_eq;

use super::{game_moves::{Move, Rock, Stick}, *};

type GameType = Game<TableGraph<Vec2>, Vec2>;

#[test]
fn stick() -> Result<(), MoveInvalid> {
    let mut game: GameType = Default::default();
    game.play_stick(Vec2(0, 0), Vec2(0, 1), 0)?
        .render(Path::new("output/test/stick.png"))
        .unwrap();
    Ok(())
}

#[test]
fn stick_move() -> Result<(), MoveInvalid> {
    let mut game: GameType = Default::default();
    let stick_move = Move::Stick(Stick {
        player: 0,
        start: Vec2(0, 0),
        end: Vec2(0, 1),
    });

    game.play_move(stick_move)?
        .render(Path::new("output/test/stick_move.png"))
        .unwrap();
    Ok(())
}

#[test]
fn rock_move() -> Result<(), MoveInvalid> {
    let mut game: GameType = Default::default();
    let rock_move = Move::Rock(Rock {
        player: 0,
        location: Vec2(0, 0),
    });

    game.play_move(rock_move)?
        .render(Path::new("output/test/rock_move.png"))
        .unwrap();

    Ok(())
}

#[test]
#[should_panic]
fn stick_start_on_opponent_rock() {
    let mut game: GameType = Default::default();
    game.play_rock(Vec2(0, 0), 1)
        .unwrap()
        .play_stick(Vec2(0, 0), Vec2(1, 1), 0)
        .unwrap()
        .render(Path::new("output/test/stick_start_on_opponent_rock.png"))
        .unwrap();
}

#[test]
#[should_panic]
fn stick_start_on_line() {
    let mut game: GameType = Default::default();
    game.play_rock(Vec2(0, 0), 1)
        .unwrap()
        .play_stick(Vec2(0, 0), Vec2(1, 1), 0)
        .unwrap()
        .render(Path::new("output/test/stick_start_on_opponent_rock.png"))
        .unwrap();
}

#[test]
#[should_panic]
fn stick_lines_overlap() {
    let mut game: GameType = Default::default();
    game.play_stick(Vec2(0, 0), Vec2(0, 1), 1)
        .unwrap()
        .play_stick(Vec2(0, 1), Vec2(1, 0), 0)
        .unwrap()
        .play_stick(Vec2(0, 0), Vec2(1, 1), 0)
        .unwrap()
        .render(Path::new("output/test/stick_lines_overlap.png"))
        .unwrap();
}

#[test]
#[should_panic]
fn stick_too_long() {
    let mut game: GameType = Default::default();
    game.play_stick(Vec2(0, 0), Vec2(2, 2), 0)
        .unwrap()
        .render(Path::new("output/test/stick_too_long.png"))
        .unwrap();
}

#[test]
/// This should work, because the area created is a unit triangle, which
/// should not end the game.
fn stick_area_found_too_small() -> Result<(), MoveInvalid> {
    let mut game: GameType = Default::default();
    game.play_stick(Vec2(0, 0), Vec2(1, 0), 0)?
        .play_stick(Vec2(1, 0), Vec2(2, 1), 1)?
        .play_stick(Vec2(1, 0), Vec2(2, 0), 0)?
        .render(Path::new("output/test/stick_area_found_too_small_1.png")).unwrap()
        .play_stick(Vec2(2, 0), Vec2(2, 1), 1)?
        .render(Path::new("output/test/stick_area_found_too_small.png"))
        .unwrap();
    Ok(())
}

#[test]
fn stick_area_found() {
    color_eyre::install().unwrap();
    let mut game: GameType = Default::default();
    let result = game
        .play_stick(Vec2(0, 0), Vec2(1, 0), 0)
        .unwrap()
        .play_stick(Vec2(0, 0), Vec2(0, 1), 1)
        .unwrap()
        .play_stick(Vec2(1, 0), Vec2(1, 1), 0)
        .unwrap()
        .play_stick(Vec2(0, 1), Vec2(1, 1), 1);
    assert_eq!(result, Err(MoveInvalid::Stick(StickInvalid::AreaCreated(1))));
}

#[test]
fn test_move_history() {
    // color_eyre::install().unwrap();
    let mut game: GameType = Default::default();
    let move_history = game
        .play_stick(Vec2(0, 0), Vec2(1, 0), 0)
        .unwrap()
        .play_stick(Vec2(0, 0), Vec2(0, 1), 1)
        .unwrap()
        .play_rock(Vec2(2, 2), 0)
        .unwrap()
        .play_stick(Vec2(1, 0), Vec2(1, 1), 1)
        .unwrap()
        .get_move_history();

    let true_move_history = vec![
        Move::Stick(Stick { player: 0, start: Vec2(0, 0), end: Vec2(1, 0) }),
        Move::Stick(Stick { player: 1, start: Vec2(0, 0), end: Vec2(0, 1) }),
        Move::Rock(Rock { player: 0, location: Vec2(2, 2)}),
        Move::Stick(Stick { player: 1, start: Vec2(1, 0), end: Vec2(1, 1) }),
    ];

    println!("{:?}", move_history);
    assert_eq!(move_history, &true_move_history);
}

#[test]
fn test_player_move_order() -> Result<(), MoveInvalid> {
    let mut game: GameType = Game::new(vec![0, 1, 2]);
    game.play_stick(Vec2(0, 0), Vec2(1, 0), 0)?
        .play_stick(Vec2(1, 0), Vec2(2, 1), 1)?
        .play_stick(Vec2(1, 0), Vec2(2, 0), 2)?
        .play_stick(Vec2(2, 0), Vec2(2, 1), 0)?;
    Ok(())
}

#[test]
#[should_panic]
fn test_player_move_order_wrong() {
    let mut game: GameType = Game::new(vec![0, 1, 2]);
    game
        .play_stick(Vec2(0, 0), Vec2(1, 0), 0)
        .unwrap()
        .play_stick(Vec2(0, 0), Vec2(0, 1), 1)
        .unwrap()
        .play_rock(Vec2(2, 2), 0) // This should be player 2, so this should panic
        .unwrap()
        .play_stick(Vec2(1, 0), Vec2(1, 1), 1)
        .unwrap();
}

#[test]
fn rock() -> Result<(), MoveInvalid> {
    let mut game: GameType = Default::default();
    game.play_rock(Vec2(0, 0), 0)?
        .render(Path::new("output/test/rock.png"))
        .unwrap();
    Ok(())
}

#[test]
#[should_panic]
fn rock_occupied() {
    let mut game: GameType = Default::default();
    game.play_rock(Vec2(0, 0), 0)
        .unwrap()
        .play_rock(Vec2(0, 0), 0)
        .unwrap()
        .render(Path::new("output/test/rock_occupied.png"))
        .unwrap();
}
