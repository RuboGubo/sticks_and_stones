use std::{fmt::Display, ops::Sub};

#[cfg(feature = "serde1")]
use serde::{Deserialize, Serialize};

/// This is an integer, as the game takes place on a grid.
#[cfg_attr(feature = "serde1", derive(Serialize, Deserialize))]
#[derive(Debug, Default, PartialEq, Eq, Hash, Clone)]
pub struct Vec2(pub i32, pub i32);

impl Display for Vec2 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.0, self.1)
    }
}

pub trait VectorMethods
where
    Self: Sized,
{
    fn is_adjacent(&self, other: &Self) -> bool;

    fn get_overlap_points(&self, self_end: &Self) -> Option<[Self; 2]>;
}

impl VectorMethods for Vec2 {
    fn is_adjacent(&self, other: &Self) -> bool {
        if self == other {
            return false;
        }

        for i in -1..=1 {
            for j in -1..=1 {
                if self.0 + i == other.0 && self.1 + j == other.1 {
                    return true;
                }
            }
        }

        false
    }

    fn get_overlap_points(&self, self_end: &Self) -> Option<[Self; 2]> {
        let difference = self_end - self;
        let target_1 = Vec2(self.0, self.1 + difference.1);
        let target_2 = Vec2(self.0 + difference.0, self.1);

        if difference.1 == 0 || difference.0 == 0 {
            return None;
        }

        Some([target_1, target_2])
    }
}

impl From<Vec2> for (f64, f64) {
    fn from(value: Vec2) -> Self {
        (value.0 as f64, value.1 as f64)
    }
}

impl Sub for Vec2 {
    type Output = Self;

    fn sub(mut self, rhs: Self) -> Self::Output {
        self.0 -= rhs.0;
        self.1 -= rhs.1;
        self
    }
}

impl Sub for &Vec2 {
    type Output = Vec2;

    fn sub(self, rhs: Self) -> Self::Output {
        let mut new = Vec2::default();
        new.0 = self.0 - rhs.0;
        new.1 = self.1 - rhs.1;
        new
    }
}
