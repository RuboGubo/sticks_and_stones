use euclid::Point2D;

pub struct GameSpace;
pub type GamePoint = Point2D<u32, GameSpace>;