#[cfg(feature = "serde1")]
use serde::{Deserialize, Serialize};

#[cfg_attr(feature = "serde1", derive(Serialize, Deserialize))]
#[derive(Debug, PartialEq)]
pub enum StickInvalid<Player> {
    TooLong,
    OpponentRockOnStart,
    StartNotOnLine,
    Overlap,
    /// u32 = wining player_id
    AreaCreated(Player), 
}

#[cfg_attr(feature = "serde1", derive(Serialize, Deserialize))]
#[derive(Debug, PartialEq)]
pub enum RockInvalid {
    PositionOccupied,
    PlacedTooManyRocks(u32),
}

#[cfg_attr(feature = "serde1", derive(Serialize, Deserialize))]
#[derive(Debug, PartialEq)]
pub enum MoveInvalid<Player> {
    Rock(RockInvalid),
    Stick(StickInvalid<Player>),
    NotPlayerTurn {
        attempted_player: Player,
        player_turn: Player,
    }
}

impl<Player> From<StickInvalid<Player>> for MoveInvalid<Player> {
    fn from(value: StickInvalid<Player>) -> Self {
        Self::Stick(value)
    }
}

impl<Player> From<RockInvalid> for MoveInvalid<Player> {
    fn from(value: RockInvalid) -> Self {
        Self::Rock(value)
    }
}
