mod graph_trait;
mod table_graph;

pub use graph_trait::UndirectedWeightedGraphTrait;
pub use table_graph::TableGraph;
