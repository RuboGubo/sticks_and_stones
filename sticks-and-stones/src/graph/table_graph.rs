use std::{collections::HashMap, fmt::Debug, hash::Hash};

#[cfg(feature = "serde1")]
use serde::{Deserialize, Serialize};

use super::graph_trait::UndirectedWeightedGraphTrait;

/// A table graph was used due to it's ability to efficiently store sparse data.
///
/// This was achieved with using a hash map under the hood, with the following
/// configuration:
/// - Key: Node (Start/Parent)
/// - Value: Tuple of
///     - Node (End/Child)
///     - Weight (Player)
///
/// The reason the Weighted Table Graph Trait was implemented was so that if the
/// data did not turn out to be sparse, the struct could easily be replaced,
/// de-coupling it from the rest of the code.
///
/// This struct saves data to the db using the following model:
///
/// Note: Hashmaps do not guarantee order.
///
/// 1. Enumerate through each key (parent node) in the hashmap, finding all it's
/// children
/// 2. This list of children is then inserted with the parent into the Graph Nodes
/// table in the db. It should be noted that the weight is not stored, as this can be
/// recomputed during run time, using whatever cost function is currently in use.
///
/// In order to load data from the db, the program requests from the database rows
/// of parent → child relations. It then loops over the list of returned rows, using
/// the add_two_way_connection function to insert them into the Graph.
#[cfg_attr(feature = "serde1", derive(Serialize, Deserialize))]
#[derive(Debug, Default, PartialEq, Clone)]
pub struct TableGraph<Node, Player>
where
    Node: PartialEq + Eq + Hash,
{
    table: HashMap<Node, Vec<(Node, Player)>>,
}

impl<Node, Player> TableGraph<Node, Player>
where
    Node: PartialEq + Eq + Hash,
{
    fn merge_extend<T: IntoIterator<Item = (Node, Vec<(Node, Player)>)>>(&mut self, iter: T) {
        let iter = iter.into_iter();
        let reserve = if self.table.is_empty() {
            iter.size_hint().0
        } else {
            (iter.size_hint().0 + 1) / 2
        };
        self.table.reserve(reserve);
        iter.for_each(|(k, v)| {
            self.table.entry(k).and_modify(|x| x.extend(v)).or_default();
        });
    }

    pub fn extend(&mut self, other_value: Self) -> &mut Self {
        self.merge_extend(other_value.table);
        self
    }
}

impl<Node: Eq + Hash, Player> IntoIterator for TableGraph<Node, Player>
where
    Node: Into<(f64, f64)>,
{
    type Item = (Node, Vec<(Node, Player)>);
    type IntoIter = std::collections::hash_map::IntoIter<Node, Vec<(Node, Player)>>;

    fn into_iter(self) -> Self::IntoIter {
        self.table.into_iter()
    }
}

// TODO: Remove the amount of clones
impl<Player: Clone, Node: Eq + Hash + Clone + Debug> UndirectedWeightedGraphTrait<Node, Player> for TableGraph<Node, Player> {
    fn add_connection(&mut self, start: &Node, end: &Node, player: &Player) -> &mut Self {
        self.table
            .entry(start.clone())
            .and_modify(|x| x.push((end.clone(), player.clone())))
            .or_insert(vec![(end.clone(), player.clone())]);

        self.table
            .entry(end.clone())
            .and_modify(|x| x.push((start.clone(), player.clone())))
            .or_insert(vec![(start.clone(), player.clone())]);

        self
    }

    fn get_children(&self, start: &Node) -> Option<&Vec<(Node, Player)>> {
        self.table.get(start)
    }

    fn get_all_parents(&self) -> Vec<&Node> {
        self.table.keys().collect()
    }
}

#[test]
fn test() {
    let temp: HashMap<u32, u32> = Default::default();

    temp.into_iter();
}
