pub trait UndirectedWeightedGraphTrait<Node, Weight>
where
    Self: Sized,
{
    fn add_connection(&mut self, start: &Node, end: &Node, weight: &Weight) -> &mut Self;

    fn get_children(&self, start: &Node) -> Option<&Vec<(Node, Weight)>>;

    fn get_all_parents(&self) -> Vec<&Node>;
}
