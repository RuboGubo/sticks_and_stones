// #![deny(warnings)]
use std::collections::HashMap;
use std::convert::Infallible;
use std::sync::atomic::AtomicU32;
use std::sync::{
    atomic::{Ordering},
    Arc,
};

use futures_util::{SinkExt, StreamExt, TryFutureExt};
use sticks_and_stones::game::{Move, Vec2};
use sticks_and_stones::graph::TableGraph;
use sticks_and_stones::Game;
use tokio::sync::{mpsc, RwLock};
use tokio_stream::wrappers::UnboundedReceiverStream;
use warp::ws::{Message, WebSocket};
use warp::Filter;
use futures_util::FutureExt;

mod room;

/// Our global unique user id counter.
static NEXT_USER_ID: AtomicU32 = AtomicU32::new(1);

/// Our state of currently connected users.
///
/// - Key is their id
/// - Value is a sender of `warp::ws::Message`
type Room = Arc<RwLock<HashMap<u32, mpsc::UnboundedSender<Message>>>>;
type Rooms = Arc<RwLock<HashMap<u32, Room>>>;

async fn get_room(room_id: u32, room: Rooms) -> Result<Room, Infallible> {
    Ok(
        room.write()
            .await
            .entry(room_id)
            .or_default()
            .clone()
    )
}

#[tokio::main]
async fn main() {
    pretty_env_logger::init();

    // Keep track of all connected users, and rooms.
    let rooms = Rooms::default();
    let rooms = warp::any().map(move || rooms.clone());
    // Turn our "state" into a new Filter...
    let room_filter = warp::path::param::<u32>()
        .and(rooms)
        .and_then(get_room);
    
    // GET /room/<room_id>/ -> websocket upgrade
    let get_room = warp::path("room")
        // The `ws()` filter will prepare Websocket handshake...
        .and(warp::ws())
        .and(room_filter)
        .map(|ws: warp::ws::Ws, room| {
            // This will call our function if the handshake succeeds.
            ws.on_upgrade(move |socket| user_connected(socket, room))
        });

    let echo = warp::path("echo")
        // The `ws()` filter will prepare the Websocket handshake.
        .and(warp::ws())
        .map(|ws: warp::ws::Ws| {
            // And then our closure will be called when it completes...
            ws.on_upgrade(|websocket| {
                // Just echo all messages back...
                let (tx, rx) = websocket.split();
                rx.map(|e| {println!("{:?}", e); e}).forward(tx).map(|result| {
                    println!("{:?}", result);
                    if let Err(e) = result {
                        eprintln!("websocket error: {:?}", e);
                    }
                })
            })
        });

    // GET / -> index html
    let index = warp::path::end().map(|| warp::reply::html(INDEX_HTML));

    let routes = index.or(get_room).or(echo);

    warp::serve(routes).run(([127, 0, 0, 1], 8000)).await;
}

async fn user_connected(ws: WebSocket, room: Room) {
    // Use a counter to assign a new unique ID for this user.
    let my_id = NEXT_USER_ID.fetch_add(1, Ordering::Relaxed);

    eprintln!("new user: {}", my_id);

    // Split the socket into a sender and receive of messages.
    let (mut user_ws_tx, mut user_ws_rx) = ws.split();

    // Use an unbounded channel to handle buffering and flushing of messages
    // to the websocket...
    let (tx, rx) = mpsc::unbounded_channel();
    let mut rx = UnboundedReceiverStream::new(rx);

    tokio::task::spawn(async move {
        while let Some(message) = rx.next().await {
            println!("103 msg");
            user_ws_tx
                .send(message)
                .unwrap_or_else(|e| {
                    eprintln!("websocket send error: {}", e);
                })
                .await;
        }
    });

    // Save the sender in our list of connected users.
    room.write().await.insert(my_id, tx);

    // Return a `Future` that is basically a state machine managing
    // this specific user's connection.

    // Every time the user sends a message, broadcast it to
    // all other users...
    while let Some(result) = user_ws_rx.next().await {
        let msg = match result {
            Ok(msg) => msg,
            Err(e) => {
                eprintln!("websocket error(uid={}): {}", my_id, e);
                break;
            }
        };
        handle_update(my_id, msg, room.clone()).await;
    }

    // user_ws_rx stream will keep processing as long as the user stays
    // connected. Once they disconnect, then...
    user_disconnected(my_id, &room).await;
}

async fn handle_update(my_id: u32, msg: Message, users: Room) {
    // Skip any non-Text messages...
    let game_move = if let Ok(s) = msg.to_str() {
        if let Ok(s) = serde_json::from_str::<Move<Vec2, u32>>(s) {
            s
        } else {
            return
        }
    } else {
        return;
    };

    // New message from this user, send it to everyone else (except same uid)...
    for (&uid, tx) in users.read().await.iter() {
        println!("{:?}", game_move);
        if my_id != uid || true {
            if let Err(_disconnected) = tx.send(Message::text(serde_json::to_string(&game_move).unwrap())) {
                // The tx is disconnected, our `user_disconnected` code
                // should be happening in another task, nothing more to
                // do here.
            }
        }
    }
}

fn process_message(user: u32, msg: Message, users: Room) -> Result<(), ()> {
    let game_move: Move<Vec2, u32> = serde_json::from_str(msg.to_str()?).map_err(|_| ())?;

    todo!();
}

async fn user_disconnected(my_id: u32, users: &Room) {
    eprintln!("good bye user: {}", my_id);

    // Stream closed up, so remove from the user list
    users.write().await.remove(&my_id);
}

static INDEX_HTML: &str = r#"<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Warp Chat</title>
    </head>
    <body>
        <h1>Warp chat</h1>
        <div id="chat">
            <p><em>Connecting...</em></p>
        </div>
        <input type="text" id="text" />
        <button type="button" id="send">Send</button>
        <script type="text/javascript">
        const chat = document.getElementById('chat');
        const text = document.getElementById('text');
        const uri = 'ws://' + location.host + '/chat';
        const ws = new WebSocket(uri);

        function message(data) {
            const line = document.createElement('p');
            line.innerText = data;
            chat.appendChild(line);
        }

        ws.onopen = function() {
            chat.innerHTML = '<p><em>Connected!</em></p>';
        };

        ws.onmessage = function(msg) {
            message(msg.data);
        };

        ws.onclose = function() {
            chat.getElementsByTagName('em')[0].innerText = 'Disconnected!';
        };

        send.onclick = function() {
            const msg = text.value;
            ws.send(msg);
            text.value = '';

            message('<You>: ' + msg);
        };
        </script>
    </body>
</html>
"#;