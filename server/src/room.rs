use std::collections::HashMap;

use networking::Message;
use sticks_and_stones::{game::Vec2, graph::TableGraph, Game};
use tokio::sync::mpsc::UnboundedSender;

pub type GameType = Game<TableGraph<Vec2, u32>, Vec2, u32>;

pub struct Room {
    game: GameType,
    players: HashMap<u32, UnboundedSender<Message>>,
}
