use euclid::Point2D;

pub struct CanvasSpace;
pub type CanvasPoint = Point2D<f64, CanvasSpace>;