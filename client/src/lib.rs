use std::sync::{Arc, Mutex};

use gloo::{events::EventListener, timers::callback::Timeout};
use grid::Grid;
use renderer::{Renderer, TestRenderObject};
use sticks_and_stones::{game::{GamePoint, Move, Rock, Stick, Vec2}, graph::TableGraph, Game};
use wasm_bindgen::prelude::*;
use web_sys::{console::log_1, js_sys::{ArrayBuffer, JsString}, Blob, CanvasRenderingContext2d, Element, HtmlCanvasElement, MessageEvent, Request, RequestInit, Response, WebSocket, Window};

mod renderer;
mod grid;
mod object;
mod points;

type GameType = Game<TableGraph<Vec2, u32>, Vec2, u32>;

fn get_board_context(canvas_element: HtmlCanvasElement) -> Result<CanvasRenderingContext2d, JsValue> {
    Ok(
        canvas_element
            .get_context("2d")?
            .ok_or("Well, could'nt get that context")?
            .dyn_into::<CanvasRenderingContext2d>()?
    )
}

// Called when the Wasm module is instantiated
#[wasm_bindgen(start)]
fn main() -> Result<(), JsValue> {
    // Use `web_sys`'s global `window` function to get a handle on the global
    // window object.
    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");
    let body = document.body().expect("document should have a body");

    let game_board_canvas = document.get_element_by_id("game_board").expect("Need a game board! (canvas element)");
    
    let game_board_dimensions = game_board_canvas
        .get_bounding_client_rect();

    let game_board_html = game_board_canvas.clone()
        .dyn_into::<HtmlCanvasElement>()?;

    let game_board_context = get_board_context(game_board_html)?;

    let renderer = Renderer::new(game_board_context, game_board_dimensions.into())
        .register_object(&TestRenderObject)
        .register_object(&Grid::new(9))
        .render_frame();

    let game: Arc<Mutex<GameType>> = Default::default();

    let new_game = game.clone();
    let move_canvas = Closure::<dyn FnMut(_)>::new(move |event: web_sys::MouseEvent| {
        if event.buttons() == 1 {
            log_1(&event.clone().offset_x().into());
            log_1(&event.clone().offset_y().into());
            log_1(&format!("{:#?}", new_game.lock()).into());
        }
    });

    game_board_canvas.add_event_listener_with_callback("mousemove", move_canvas.as_ref().unchecked_ref())?;
    move_canvas.forget();

    // let mut opts = RequestInit::new();
    // opts.set_method("GET");

    // // Create a new Request object
    // let request = Request::new_with_str_and_init("http://127.0.0.1:8000/room/1", &opts).unwrap();

    // // Fetch the request and return a Promise
    // let promise = window.fetch_with_request(&request);

    // // Convert the Promise to handle the response
    // let future = promise.then(&mut |response_value| {
    //     let response: Response = response_value.dyn_into().unwrap();
        
    //     // Check if the response is OK
    //     if response.ok() {
    //         // Convert the response to text
    //         let text_promise = response.text().unwrap();
    //         text_promise.then(&mut |text_value| {
    //             // Resolve the promise with the text
    //             Ok(text_value)
    //         })
    //     } else {
    //         // Reject the promise if the response is not OK
    //         Promise::reject(&JsValue::from_str("Failed to fetch data"))
    //     }
    // });
    let ws = WebSocket::new("http://0.0.0.0:8000/room/1")?;

    EventListener::new(&ws, "message", move |event| {
        if let Ok(txt) = event
            .dyn_ref::<web_sys::MessageEvent>()
            .unwrap_throw()
            .data()
            .dyn_into::<JsString>() 
        {
            log_1(&format!("message event, received Text: {:?}", txt).into());
            let txt: String = txt.into();
            if let Ok(game) = serde_json::from_str::<GameType>(&txt) {
                log_1(&format!("{}", game).into())
            }
        }
    }).forget();

    let cloned_ws = ws.clone();
    let cloned_game = game.clone();

    EventListener::new(&ws, "open", move |_event| {
        log_1(&format!("socket opened").into());

        let mut moves: Vec<Move<Vec2, u32>> = vec![
            Stick::new(0, Vec2(0, 0), Vec2(1, 0)).into(),
            Stick::new(1, Vec2(0, 0), Vec2(0, 1)).into(),
            Rock::new(0, Vec2(2, 2)).into(),
            Stick::new(1, Vec2(1, 0), Vec2(1, 1)).into(),
        ];
        moves.reverse();

        recursive_msg(cloned_ws.clone(), moves);

    }).forget();

    Ok(())
}

fn recursive_msg(ws: WebSocket, mut moves: Vec<Move<Vec2, u32>>) {
    if moves.len() == 0 {
        return
    }

    match ws.send_with_str(&serde_json::to_string(&moves.pop()).unwrap()) {
        Ok(_) => log_1(&format!("message successfully sent").into()),
        Err(err) => log_1(&format!("error sending message: {:?}", err).into()),
    }

    Timeout::new(5_000, move || {
        recursive_msg(ws, moves);
    }).forget();
}

#[wasm_bindgen]
pub fn add(a: u32, b: u32) -> u32 {
    a + b
}

