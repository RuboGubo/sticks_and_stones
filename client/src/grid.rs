use web_sys::DomRectReadOnly;

use crate::renderer::{Context, RenderError, Renderable};

pub struct Grid {
    spacing: u32
}

impl Grid {
    pub fn new(spacing: u32) -> Self {
        Self {
            spacing
        }
    }
}

fn line(ctx: &Context, sx: f64, sy: f64, ex: f64, ey: f64) -> &Context {
    ctx.move_to(sx, sy);
    ctx.line_to(ex, ey);
    ctx
}

impl Renderable for Grid {
    fn render(&self, ctx: &Context, rect: &DomRectReadOnly) -> Result<(), RenderError> {
        ctx.begin_path();
        for height in (0..rect.height() as u32).step_by(self.spacing as usize) {
            line(ctx, 0., height as f64, rect.width() as f64, height as f64);
        }
        for width in (0..rect.width() as u32).step_by(self.spacing as usize) {
            line(ctx, width as f64, 0., width as f64, rect.width() as f64);
        }
        ctx.stroke();

        Ok(())
    }
}
