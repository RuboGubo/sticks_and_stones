use std::f64::consts::PI;

use web_sys::{CanvasRenderingContext2d, DomRectReadOnly};

pub type Context = CanvasRenderingContext2d;

pub enum RenderError {
    Error
}

pub struct Renderer<'a> {
    context: Context,
    canvas_rect: DomRectReadOnly,
    render_objects: Vec<&'a dyn Renderable>,
}

impl<'a> Renderer<'a> {
    pub fn new(context: Context, canvas_rect: DomRectReadOnly) -> Self {
        Self {
            context,
            canvas_rect,
            render_objects: Vec::new(),
        }
    }

    pub fn render_frame(&self) -> Result<&Self, RenderError>  {
        for object in &self.render_objects {
            self.context.save();

            object.render(&self.context, &self.canvas_rect)?;

            self.context.restore();
        }

        Ok(self)
    }

    pub fn register_object(&'a mut self, object: &'a dyn Renderable) -> &'a mut Self {
        self.render_objects.push(object);
        self.render_objects.sort_by_key(|k| k.get_index());
        self
    }
}

pub trait Renderable
{
    fn render(&self, context: &Context, canvas_rect: &DomRectReadOnly) -> Result<(), RenderError>;

    fn get_index(&self) -> u32 {
        0
    }
}

pub struct TestRenderObject;

impl Renderable for TestRenderObject {
    fn render(&self, context: &Context, _: &DomRectReadOnly) -> Result<(), RenderError> {
        context.begin_path();

        context
            .arc(0.0, 0.0, 50.0, 0.0, PI * 2.0)
            .unwrap();
        context.stroke();

        Ok(())
    }
}
