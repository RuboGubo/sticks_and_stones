with import <nixpkgs> {};
mkShell {
  buildInputs = [
    pkg-config
    cmake
    openssl
    fontconfig
  ];
}
